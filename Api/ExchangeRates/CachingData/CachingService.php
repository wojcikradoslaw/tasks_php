<?php

namespace Api\ExchangeRates\CachingData;


use Api\ExchangeRates\DownloadingDataFromApi\DownloadDataFromApiInterface;
use Api\ExchangeRates\Messeges\Message;
use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\Messeges\MessageType;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;


class CachingService
{

    /**
     * Nazwa klucza, przypisujemy poprzez konstruktor przy tworzeniu obiektu
     * @var string
     */
    private $cacheKey;

    /**
     * Obiekt zbierania komunikatów systemowych
     * @var Messages
     */
    private $messages;

    /**
     * Czas życia danych klucz-wartość w systemie cache
     * @var int
     */
    private $keyLifetime = 120;

    /**
     * @var FilesystemAdapter
     */
    private $cache;

    /**
     * @var DownloadDataFromApiInterface
     */
    private $downloadingDataFromApi;

    public function __construct(string $cacheKey, DownloadDataFromApiInterface $downloadingDataFromApi)
    {
        $this->downloadingDataFromApi = $downloadingDataFromApi;
        $this->cacheKey = $cacheKey;
        $this->messages = Messages::getInstance();
        $this->cache = new FilesystemAdapter();
    }

    public function getData()
    {

        $item = $this->cache->getItem($this->cacheKey);

        if (!$item->isHit()) {

            $item->expiresAfter($this->keyLifetime);

            $response = $this->downloadingDataFromApi->getDataFromApi();


            if ($response->getStatusCode() == 200) {

                $item->set($response->toArray());
                $this->cache->save($item);
                $this->messages->setMessage(new Message('Downloading the NBP data from the API', new MessageType(MessageType::INFO)));

            } else {
                $this->messages->setMessage(new Message('API data download error', new MessageType(MessageType::SYSTEM_ERROR)));
            }

        } else {
            $this->messages->setMessage(new Message('Downloading the NBP table data from the cache', new MessageType(MessageType::INFO)));
        }

        return $item->get();

    }


}