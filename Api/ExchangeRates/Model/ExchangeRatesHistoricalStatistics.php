<?php

namespace Api\ExchangeRates\Model;

use Api\ExchangeRates\CachingData\CachingService;
use Api\ExchangeRates\DownloadingDataFromApi;
use Api\ExchangeRates\Messeges\Message;
use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\Messeges\MessageType;
use Api\ResponseInterface;


class ExchangeRatesHistoricalStatistics
{

    private $downloadData;
    private $tableNBP = [];


    private $dateFrom;
    private $dateTo;

    /**
     * Currency code from NBP Table
     * @var string
     */

    private $currency;

    private $highestExchangeRateInPeriod = 0.0;
    private $dateHighestExchangeRateInPeriod;

    private $lowestExchangeRateInPeriod = 0.0;
    private $dateLowestExchangeRateInPeriod;

    private $averageExchangeRateInPeriod = 0.0;

    /**
     * @var Messages
     */
    private $messages;

    public function __construct($inputArray)
    {
        $this->messages = Messages::getInstance();

        $this->setGet($inputArray);

        $this->getDataNBP();

        if (!$this->messages->isSystemError()) { //Odpytanie obiektu $messages czy nie ma błędów związanych z tabelą NBP

            $this->calculate();

        }

    }

    private function setGet($inputArray)
    {
        $this->dateFrom = $inputArray['dateFrom'];
        $this->dateTo = $inputArray['dateTo'];
        $this->currency = $inputArray['currency'];

    }

    private function getDataNBP()
    {

        $cacheKey = 'NBP-A-From-' . $this->dateFrom . '-To-' . $this->dateTo;

        $this->downloadData = (new CachingService($cacheKey,
            new DownloadingDataFromApi\DownloadDataExchangeRatesHistoricalStatisticsApi($this->dateFrom, $this->dateTo))
        )->getData();

    }

    private function calculate()
    {
        $sumOfCurrenciesInThePeriod = 0;
        $this->tableNBP['mid'] = 0;

        foreach ($this->downloadData as $key => $table) {

            foreach ($table['rates'] as $currency) {

                if ($currency['code'] == $this->currency) {
                    $this->tableNBP['mid'] = $currency['mid'];
                    $sumOfCurrenciesInThePeriod += (float)$currency['mid'];
                }
            }

            $this->tableNBP['no'] = $table['no'];
            $this->tableNBP['effectiveDate'] = $table['effectiveDate'];



            $this->findMaxExchangeRate($this->tableNBP);
            $this->findMinExchangeRate($this->tableNBP, $key);

        }

        if ($this->tableNBP['mid'] == 0) {
            $this->messages->setMessage(new Message('No currency named ' . $this->currency . ' found in the NBP table.', new MessageType(MessageType::SYSTEM_ERROR)));
        }

        $this->calculateAverageExchangeRateInPeriod($sumOfCurrenciesInThePeriod, count($this->downloadData));


    }

    private function findMaxExchangeRate(array $table)
    {

        if ($table['mid'] >= $this->highestExchangeRateInPeriod) {
            $this->highestExchangeRateInPeriod = $table['mid'];
            $this->dateHighestExchangeRateInPeriod = $table['effectiveDate'];
        }

    }

    private function findMinExchangeRate(array $table, $key)
    {
        if ($key == 0) {
            $this->lowestExchangeRateInPeriod = $table['mid'];
        }

        if ($table['mid'] < $this->lowestExchangeRateInPeriod) {
            $this->lowestExchangeRateInPeriod = $table['mid'];
            $this->dateLowestExchangeRateInPeriod = $table['effectiveDate'];
        }


    }

    private function calculateAverageExchangeRateInPeriod($sumOfCurrenciesInThePeriod, $numberOfTables)
    {
        $this->averageExchangeRateInPeriod = $sumOfCurrenciesInThePeriod / $numberOfTables;
    }

    public function returnResponse(): ResponseInterface
    {

        $response = new PreparingResponseForExchangeRatesHistoricalStatistics(
            $this->dateFrom,
            $this->dateTo,
            $this->currency,
            $this->highestExchangeRateInPeriod,
            $this->dateHighestExchangeRateInPeriod,
            $this->lowestExchangeRateInPeriod,
            $this->dateLowestExchangeRateInPeriod,
            $this->averageExchangeRateInPeriod
        );

        return $response;

    }


}