<?php


namespace Api\ExchangeRates\Model;


class FormattingTheOutputData
{

    /**
     * @var float
     */
    private $value;

    /**
     * @var int
     */
    private $numberOfRoundingPlaces;

    public function __construct(float $value, int $numberOfRoundingPlaces = 5)
    {
        $this->value = $value;
        $this->numberOfRoundingPlaces = $numberOfRoundingPlaces;
    }

    public function calculate(): float
    {
        return number_format(floatval($this->value), $this->numberOfRoundingPlaces, '.', '');
    }


}