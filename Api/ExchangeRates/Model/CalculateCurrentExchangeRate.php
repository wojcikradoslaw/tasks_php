<?php

namespace Api\ExchangeRates\Model;

use Api\ExchangeRates\CachingData\CachingService;
use Api\ExchangeRates\DownloadingDataFromApi;
use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\ValidateData\ValidateInputData;
use Api\ResponseInterface;


class CalculateCurrentExchangeRate
{


    private $downloadData;
    private $tableNBP = [];
    private $resultingRate = 0;
    private $amount = 0;
    private $resultingAmount = 0;
    private $fromCurrency = '';
    private $toCurrency = '';


    public function __construct($inputArray)
    {
        $messages = Messages::getInstance();

        $this->getDataNBP();

        if (!$messages->isSystemError()) { //Odpytanie obiektu $messages czy nie ma błędów związanych z tabelą NBP

            $this->prepareTable();

            new ValidateInputData($this->tableNBP, $inputArray);

            if (!$messages->isSystemError()) { //Odpytanie obiektu $messages czy nie ma błędów związanych z danymi wejściowymi

                $this->setGet($inputArray);

                $this->calculateResultingRate();
                $this->calculateResultingAmount();
            }

        }

    }

    private function getDataNBP()
    {
        $cacheKey = 'NBP-A-' . date('Y-m-d');
        $this->downloadData = (new CachingService($cacheKey,
            new DownloadingDataFromApi\DownloadDataFromNBPtableAapi())
        )->getData();

    }

    private function prepareTable()
    {

        foreach ($this->downloadData[0]['rates'] as $currency) {
            $this->tableNBP['Rates'][$currency['code']] = $currency['mid'];
        }
        $this->tableNBP['no'] = $this->downloadData[0]['no'];
        $this->tableNBP['effectiveDate'] = $this->downloadData[0]['effectiveDate'];

    }

    private function setGet($inputArray)
    {

        $this->fromCurrency = $inputArray['fromCurrency'];
        $this->toCurrency = $inputArray['toCurrency'];
        if (isset($inputArray['amount'])) {
            $this->amount = (float)$inputArray['amount'];
        }


    }

    private function calculateResultingRate()
    {

        $this->resultingRate = $this->tableNBP['Rates'][$this->fromCurrency] / $this->tableNBP['Rates'][$this->toCurrency];

    }

    private function calculateResultingAmount()
    {

        $this->resultingAmount = (new FormattingTheOutputData(
            $this->amount * $this->resultingRate)
        )->calculate();
    }

    public function returnResponse(): ResponseInterface
    {

        $response = new PreparingResponseForCalculateCurrentExchangeRate(
            $this->fromCurrency,
            $this->toCurrency,
            $this->resultingRate,
            $this->amount,
            $this->resultingAmount,
            $this->tableNBP
        );

        return $response;

    }


}