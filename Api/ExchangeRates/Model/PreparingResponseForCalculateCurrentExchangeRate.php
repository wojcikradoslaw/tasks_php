<?php

namespace Api\ExchangeRates\Model;

use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\Messeges\MessageType;
use Api\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;


class PreparingResponseForCalculateCurrentExchangeRate implements ResponseInterface
{

    /**
     * @var string
     */
    private $fromCurrency;
    /**
     * @var string
     */
    private $toCurrency;
    /**
     * @var float
     */
    private $resultingRate;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var float
     */


    /**
     * @var array
     */

    private $tableNBP;

    private $resultingAmount;
    /**
     * @var array
     */
    private $content;


    private $httpCode = Response::HTTP_OK;


    public function __construct(

        string $fromCurrency,
        string $toCurrency,
        float $resultingRate,
        float $amount,
        float $resultingAmount,
        array $tableNBP
    )
    {

        $this->fromCurrency = $fromCurrency;
        $this->toCurrency = $toCurrency;
        $this->resultingRate = $resultingRate;
        $this->amount = $amount;
        $this->resultingAmount = $resultingAmount;
        $this->tableNBP = $tableNBP;

        $this->setContent();
    }


    private function setContent(): void
    {

        $systemMessages = [];

        foreach (Messages::getInstance()->getMessage() as $systemMesage) {

            $systemMessages[] = $systemMesage->getContent();
            if ($systemMesage->getType() == MessageType::SYSTEM_ERROR) {

                $this->setHttpCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

        }

        if ($this->httpCode != Response::HTTP_INTERNAL_SERVER_ERROR) {

            $contentArray =

                ['data' =>
                    [
                        'fromCurrency' => $this->fromCurrency,
                        'toCurrency' => $this->toCurrency,
                        'resultingRate' => (new FormattingTheOutputData($this->resultingRate))->calculate(),
                    ],
                    'messagesAboutNBPtable' => [
                        'exchangeRatesTableNumber' => $this->tableNBP['no'],
                        'effectiveDate' => $this->tableNBP['effectiveDate']
                    ],
                    'systemMessages' => $systemMessages

                ];

            if ($this->amount > 0) {
                $contentArray['data']['amount'] = $this->amount;
                $contentArray['data']['resultingAmount'] = (new FormattingTheOutputData($this->resultingAmount))->calculate();
            }

            $this->content = $contentArray;

        } else {
            $this->content =
                [
                    'systemMessages' => $systemMessages

                ];
        }

    }

    public function getContent(bool $jsonEncoded = true)
    {
        return $jsonEncoded ? \json_encode($this->content) : $this->content;

    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function setHttpCode($httpCode): void
    {
        $this->httpCode = $httpCode;
    }
}