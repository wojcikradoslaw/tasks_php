<?php

namespace Api\ExchangeRates\Model;

use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\Messeges\MessageType;
use Api\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;


class PreparingResponseForExchangeRatesHistoricalStatistics implements ResponseInterface
{

    /**
     * @var array
     */
    private $content;


    private $httpCode = Response::HTTP_OK;
    private $dateFrom;
    private $dateTo;
    private $currency;
    private $highestExchangeRateInPeriod;
    private $dateHighestExchangeRateInPeriod;
    private $lowestExchangeRateInPeriod;
    private $dateLowestExchangeRateInPeriod;
    private $averageExchangeRateInPeriod;


    public function __construct(
        $dateFrom,
        $dateTo,
        $currency,
        $highestExchangeRateInPeriod,
        $dateHighestExchangeRateInPeriod,
        $lowestExchangeRateInPeriod,
        $dateLowestExchangeRateInPeriod,
        $averageExchangeRateInPeriod
    )
    {

        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->currency = $currency;
        $this->highestExchangeRateInPeriod = $highestExchangeRateInPeriod;
        $this->dateHighestExchangeRateInPeriod = $dateHighestExchangeRateInPeriod;
        $this->lowestExchangeRateInPeriod = $lowestExchangeRateInPeriod;
        $this->dateLowestExchangeRateInPeriod = $dateLowestExchangeRateInPeriod;
        $this->averageExchangeRateInPeriod = $averageExchangeRateInPeriod;

        $this->setContent();

    }


    private function setContent(): void
    {

        $systemMessages = [];

        foreach (Messages::getInstance()->getMessage() as $systemMesage) {

            $systemMessages[] = $systemMesage->getContent();
            if ($systemMesage->getType() == MessageType::SYSTEM_ERROR) {

                $this->setHttpCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

        }

        if ($this->httpCode != Response::HTTP_INTERNAL_SERVER_ERROR) {

            $contentArray =

                ['data' =>
                    [
                        'dateFrom' => $this->dateFrom,
                        'dateTo' => $this->dateTo,
                        'currencyFrom' => $this->currency,
                        'currencyTo' => 'PLN',
                        'highestExchangeRateInPeriod' => $this->highestExchangeRateInPeriod,
                        'dateHighestExchangeRateInPeriod' => $this->dateHighestExchangeRateInPeriod,
                        'lowestExchangeRateInPeriod' => $this->lowestExchangeRateInPeriod,
                        'dateLowestExchangeRateInPeriod' => $this->dateLowestExchangeRateInPeriod,
                        'averageExchangeRateInPeriod' => (new FormattingTheOutputData($this->averageExchangeRateInPeriod))->calculate()


                    ],
                    'systemMessages' => $systemMessages

                ];


            $this->content = $contentArray;

        } else {
            $this->content =
                [
                    'systemMessages' => $systemMessages

                ];
        }

    }

    public function getContent(bool $jsonEncoded = true)
    {
        return $jsonEncoded ? \json_encode($this->content) : $this->content;

    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function setHttpCode($httpCode): void
    {
        $this->httpCode = $httpCode;
    }
}