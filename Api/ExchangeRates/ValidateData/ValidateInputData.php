<?php

namespace Api\ExchangeRates\ValidateData;

use Api\ExchangeRates\Messeges\Message;
use Api\ExchangeRates\Messeges\Messages;
use Api\ExchangeRates\Messeges\MessageType;

class ValidateInputData
{

    /**
     * @var array
     */
    private $tableNBP;
    /**
     * @var array
     */
    private $inputArray;

    private $messages;

    public function __construct(array $tableNBP, array $inputArray)
    {
        $this->tableNBP = $tableNBP;
        $this->inputArray = $inputArray;

        $this->messages = Messages::getInstance();
        $this->checkValidCurrencyName();
        $this->checkValidAmount();


    }

    private function checkValidCurrencyName()
    {
        if (!array_key_exists($this->inputArray['fromCurrency'], $this->tableNBP['Rates'])) {
            $this->messages->setMessage(
                new Message('No currency named ' . $this->inputArray['fromCurrency'] . ' found in the NBP table.',
                    new MessageType(MessageType::SYSTEM_ERROR))
            );
        }

        if (!array_key_exists($this->inputArray['toCurrency'], $this->tableNBP['Rates'])) {
            $this->messages->setMessage(
                new Message('No currency named ' . $this->inputArray['toCurrency'] . ' found in the NBP table.',
                    new MessageType(MessageType::SYSTEM_ERROR)));
        }


    }

    private function checkValidAmount()
    {
        if (isset($this->inputArray['amount'])) {

            if ((float)$this->inputArray['amount'] <= 0) {
                $this->messages->setMessage(
                    new Message('Amount should be a number greater than zero.',
                        new MessageType(MessageType::SYSTEM_ERROR)));

            }
        }
    }

}