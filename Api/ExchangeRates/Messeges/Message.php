<?php

namespace Api\ExchangeRates\Messeges;

class Message
{

    /**
     * @var string
     */
    private $content;

    /**
     * @var MessageType
     */
    private $type;

    public function __construct(string $content, MessageType $type)
    {
        $this->content = $content;
        $this->type = $type;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getType(): string
    {
        return $this->type;
    }

}