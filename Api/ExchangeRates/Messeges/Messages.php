<?php

namespace Api\ExchangeRates\Messeges;

final class Messages
{

    /**
     * @var Messages
     */
    private static $instance = null;

    /**
     * @var array
     */
    private $messages = [];

    private function __construct()
    {
    }

    public static function getInstance(): Messages
    {
        if (self::$instance === null) {
            self::$instance = new Messages();
        }

        return self::$instance;
    }

    public function setMessage(Message $message): void
    {
        $this->messages[] = $message;
    }

    public function getMessage(): array
    {
        return $this->messages;
    }

    public function isSystemError(): bool
    {

        foreach ($this->messages as $message) {
            if ($message->getType() == MessageType::SYSTEM_ERROR) {
                return true;
            }
        }
        return false;

    }

    public function __clone()
    {
        throw new \Exception('Config is singleton - it cannot be cloned');
    }

    public function __wakeup()
    {
        throw new \Exception('Config is singleton - it cannot be unserialized');
    }

    public function __unserialize(array $data)
    {
        throw new \Exception('Config is singleton - it cannot be unserialized');
    }

}