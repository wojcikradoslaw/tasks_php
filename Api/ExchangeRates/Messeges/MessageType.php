<?php


namespace Api\ExchangeRates\Messeges;


class MessageType
{

    public const INFO = 'info';
    public const SYSTEM_ERROR = 'system_error';

    /**
     * @var string
     */
    private $name = '';

    public function __construct(string $name)
    {
        $this->name = $name;
    }


    public function __toString(): string
    {
        return $this->name;
    }

}