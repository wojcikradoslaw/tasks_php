<?php
declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\ExchangeRates\Model\CalculateCurrentExchangeRate;
use Api\ExchangeRates\Model\ExchangeRatesHistoricalStatistics;
use Api\ResponseInterface;

class CalculatorController
{

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
        return (new CalculateCurrentExchangeRate($inputArray))->returnResponse();
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {
        return (new ExchangeRatesHistoricalStatistics($inputArray))->returnResponse();
    }

}