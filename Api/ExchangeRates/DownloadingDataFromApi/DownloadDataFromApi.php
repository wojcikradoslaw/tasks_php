<?php

namespace Api\ExchangeRates\DownloadingDataFromApi;

use Symfony\Component\HttpClient\HttpClient;

abstract class DownloadDataFromApi implements DownloadDataFromApiInterface
{
    protected $apiAddress = '';

    private $httpClient;

    public function __construct()
    {

        $this->httpClient = HttpClient::create();

    }

    public function setApiAddress(string $apiAddress): void
    {
        $this->apiAddress = $apiAddress;
    }

    public function getDataFromApi()
    {

        $response = $this->httpClient->request(
            'GET',
            $this->apiAddress,
            ['headers' => ['Accept' => 'application/json']]
        );

        return $response;
    }
}