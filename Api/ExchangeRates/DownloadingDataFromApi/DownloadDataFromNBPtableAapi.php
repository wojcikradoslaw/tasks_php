<?php

namespace Api\ExchangeRates\DownloadingDataFromApi;


class DownloadDataFromNBPtableAapi extends DownloadDataFromApi implements DownloadDataFromApiInterface
{

    public function __construct()
    {
        parent::__construct();
        $this->setApiAddress('http://api.nbp.pl/api/exchangerates/tables/a');
    }


}