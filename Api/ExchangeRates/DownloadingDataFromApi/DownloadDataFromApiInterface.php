<?php

namespace Api\ExchangeRates\DownloadingDataFromApi;

interface DownloadDataFromApiInterface
{

    public function setApiAddress(string $apiAddress): void;

    public function getDataFromApi();
}
