<?php

namespace Api\ExchangeRates\DownloadingDataFromApi;


class DownloadDataExchangeRatesHistoricalStatisticsApi extends DownloadDataFromApi implements DownloadDataFromApiInterface
{

    public function __construct($dateFrom, $dateTo)
    {
        parent::__construct();
        $this->setApiAddress('https://api.nbp.pl/api/exchangerates/tables/a/' . $dateFrom . '/' . $dateTo . '/');
    }


}