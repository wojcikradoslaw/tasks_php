<?php

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    3
 * Actual Development Time [in hours]:      8
 * Your thoughts and comments:
 *
 * Przyjęte założenia do projektu:
 *  1. Sprawdzanie czy API NBP zwraca tabele. Jeżeli nie uda się odpytać API NPB to nasz program zwraca komunikat o błędzie
 *  2. Sprawdzenie kluczy danych wejściowych w tablicy $_GET (np. waluty które nie istnieją w tabeli)
 *  3. Sprawdzenie czy wartości symboli walut fromCurrency i toCurrency są poprawne, poprzez porównanie z tabelą
 *  4. Zapisywanie zdarzeń do Messages
 *  5. Przy pobieraniu danych z API NBP zastowałem cache wyników, co poprawia szybkość działania, jako
 *      klucz cache stosuję datę  w połączeniu z nazwą czyli np. NBP-A-2021-12-26
 *  6. Wynik zawiera również informacje o tabeli NBP "messagesAboutNBPtable": numer tabeli i z którego dnia
 *  7. Wartość domyślna czasu życia cache ustawiona w klasie CachingService $keyLifetime

 * ============================================
 * Feedback zadania:
 *
 * Zadanie bardzo ciekawe. Przyznam się że wykonywałem z przyjemnością. Założony czas realizacji został przekroczony,
 * ponieważ zacząłem dodawać nowe funkcjonalności (komunikaty, cache) i refaktoryzować.
 *
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */



/**
 * Obsługa wywołań przez URL:
 *
 * 1. Dane domyślne (warunek poniżej):
 *
 *      http://rtbhouse.local/?taskNr=1
 *
 * 2. Obliczenie "resultingRate" pomiędzy zadanymi walutami "fromCurrency" i "toCurrency"
 *
 *      http://rtbhouse.local/?taskNr=1&fromCurrency=USD&toCurrency=CAD
 *
 * 3. Obliczenie "resultingRate" pomiędzy zadanymi walutami "fromCurrency" i "toCurrency", oraz "resultingAmount" dla zadanej stawki "amount"
 *
 *     http://rtbhouse.local/?taskNr=1&fromCurrency=USD&toCurrency=CAD&amount=444
 *
 * 4. Przy podaniu wadliwej waluty toCurrency=CkAD - ustawiamy nagłówek 500 i zwracamy tylko 'systemMessages'
 *
 *    http://rtbhouse.local/?taskNr=1&fromCurrency=USD&toCurrency=CkAD&amount=444
 */

if( !isset($_GET['fromCurrency']) && !isset($_GET['toCurrency']) ){


    $_GET = [
        'fromCurrency' => 'USD',
        'toCurrency' => 'GBP',
        'amount' => 50,
    ];
}

$controller = new \Api\ExchangeRates\CalculatorController();
$response = $controller->calculateCurrentExchangeRateAction($_GET);




//// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
