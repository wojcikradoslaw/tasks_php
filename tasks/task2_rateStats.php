<?php

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    1
 * Actual Development Time [in hours]:      1
 * Your thoughts and comments:              Fajne zadanie, wykorzystałem klasy z zadania 1 :)
 *                                          Zastosowałem cachowanie wybranych poprzez GET  zakresów by nie odpytawać za często API NBP
 * ============================================
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */

/**
 *  1. Wywołanie obliczeń dla danych domyślnych podanych w pliku task2_rateStats.php:
 *
 *   http://rtbhouse.local/?taskNr=2
 *
 *  2. Wywołanie dla zakresów podanych w tablicy $_GET:
 *
 *   http://rtbhouse.local/?taskNr=2&dateFrom=2021-12-01&dateTo=2021-12-24&currency=USD
 *
 *  3. Gdy zastosujemy błędne wywołania np. tabele NBP których jeszcze nie ma, wyświetlamy systemMessages = "API data download error"
 *
 *   http://rtbhouse.local/?taskNr=2&dateFrom=2021-12-01&dateTo=2021-12-30&currency=USD
 *
 *  4. Gdy podamy błędną walutę, wyświetlamy komunikat "No currency named CADd found in the NBP table."
 *
 *    http://rtbhouse.local/?taskNr=2&dateFrom=2021-12-01&dateTo=2021-12-21&currency=CADd
 *
 *  5. Wartość domyślna czasu życia cache ustawiona w klasie CachingService $keyLifetime
 *
 *  6. Ograniczenie ze strony api.nbp.pl : "przy czym pojedyncze zapytanie nie może obejmować przedziału dłuższego, niż 93 dni."
 *
 *
 * Feedback zadania:
 *
 * Zadanie również bardzo ciekawe. Kontynuacja zadania task1. Wykorzystałem dużo funkcjonalności z poprzedniego zadania.
 * Nie zdąrzyłem jeszcze go uporządkować ale czas goni i przesyłam to co mam.
 * Wrzucam do cache cały zakres wybranych w $_GET dat. Dla tego wybranego zakresu można już bez odpytywania API NBP
 * dokonywać obliczeń dla róznych walut.
 *
 * Można było by się pobawić i wrzucać każdą tabelę NBP z tego zakresu pod oddzielnym kluczem (klasy mam pod takie rozwiązanie przygotowane)
 * ale już też nie starczyło mi czasu.
 *
 * Podsumowując bardzo fajne i ciekawe zadania.
 *
 */


if( !isset($_GET['dateFrom']) || !isset($_GET['dateTo']) || !isset($_GET['currency']) ) {

    $_GET = [
        'dateFrom' => '2020-08-01',
        'dateTo' => '2020-09-01',
        'currency' => 'GBP',
    ];

}

$controller = new \Api\ExchangeRates\CalculatorController();
$response = $controller->getRateStatsAction($_GET);


// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);